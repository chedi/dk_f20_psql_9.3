FROM chedi/f20_micro
MAINTAINER Chedi Toueiti <chedi.toueiti@gmail.com>

ADD start.sh /start.sh

RUN yum install postgresql-server postgresql-contrib           \
        postgresql postgis                                     \
        -y                                                  && \
                                                               \
    yum clean all                                           && \
    yum history new                                         && \
    truncate -c -s 0 /var/log/yum.log                       && \
    rm -fr /usr/share/docs                                  && \
    mv /usr/share/locale/en_US /tmp                         && \
    rm -fr /usr/share/locale/*                              && \
    mv /tmp/en_US /usr/share/locale/                        && \
    rm -fr /var/lib/yum/yumdb                               && \
    rm -fr var/lib/rpm/Packages/*                           && \
                                                               \
    chmod 0755 /start.sh

EXPOSE 5432

CMD ["/start.sh"]